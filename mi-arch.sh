pacman -Syy

pacman -Syu reflector
echo -e "Mirror Country: \c "
read country
reflector --verbose --country "$country" -f 10 --sort rate --save /etc/pacman.d/mirrorlist

pacman -Syu git

timedatectl set-ntp true
timedatectl timesync-status
timedatectl set-timezone Asia/Taipei

mkdir -p /usr/local/scripts && cd "$_"
git clone https://gitlab.com/miyorineko/minorinworker.git
cd minorinworker

pacman -S python-pip

pip install hug -U
pip install -r requirements.txt

python init.py

pacman -Syu v2ray

systemctl daemon-reload
systemctl start minoworker