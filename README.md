# Project Satsuki
## Convert to Arch
```
wget http://tinyurl.com/vps2arch
chmod +x vps2arch
./vps2arch
```

## V2Ray Install
apt-get install curl -y && curl -O https://miyorineko.github.io/v2.sh && bash v2.sh
## Telegraf Node (D)
wget https://miyorineko.github.io/telegraf.conf -O /etc/telegraf/telegraf.conf
## V2Ray Direct Install
bash <(curl -L -s https://install.direct/go.sh)
## Minimum install no root cert
apt-get install ca-certificates
## Install Minorin Worker
### Arch
```
pacman -Syu curl --noconfirm && bash <(curl -L -s https://gitlab.com/miyorineko/project-satsuki/raw/master/mi-arch.sh)
```
### Deb
```
apt install curl -y && bash <(curl -L -s https://gitlab.com/miyorineko/project-satsuki/raw/master/mi-worker-deb.sh)
```
### Yum
```
yum install curl -y && bash <(curl -L -s https://gitlab.com/miyorineko/project-satsuki/raw/master/mi-install-ct.sh)
```
## Install Minorin Worker (Re)
### Arch
```
rm -rf /usr/local/scripts/minorinworker && rm /etc/v2ray/config.json && pacman -Syu curl --noconfirm && bash <(curl -L -s https://gitlab.com/miyorineko/project-satsuki/raw/master/mi-arch.sh)
```
### Deb
```
rm -rf /usr/local/scripts/minorinworker && rm /etc/v2ray/config.json && apt install curl -y && bash <(curl -L -s https://gitlab.com/miyorineko/project-satsuki/raw/master/mi-worker-deb.sh)
```
### Yum
```
rm -rf /usr/local/scripts/minorinworker && rm /etc/v2ray/config.json && yum install curl -y && bash <(curl -L -s https://gitlab.com/miyorineko/project-satsuki/raw/master/mi-install-ct.sh)
```
## MinorinWorker Manual Update
```
cd /usr/local/scripts/minorinworker && python3 updater.py
```
## init
```
cd /usr/local/scripts/minorinworker && python3 init.py
```
## BBR
```
modprobe tcp_bbr
lsmod | grep tcp_bbr
sysctl net.ipv4.tcp_congestion_control=bbr
```