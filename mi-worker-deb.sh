apt update
apt install git dbus systemd curl -y
apt install tzdata build-essential zlib1g-dev libncurses5-dev libgdbm-dev \ 
libnss3-dev libssl-dev libreadline-dev libffi-dev -y
apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
curl https://pyenv.run | bash
pyenv install 3.7.7
pyenv global 3.7.7
timedatectl set-timezone Asia/Taipei
mkdir -p /usr/local/scripts && cd "$_"
git clone https://gitlab.com/miyorineko/minorinworker.git
cd minorinworker
pip3 install hug -U
pip3 install -r requirements.txt
python3 init.py
bash <(curl -L -s https://install.direct/go.sh)
systemctl daemon-reload
systemctl start minoworker
