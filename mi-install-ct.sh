yum update
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum install -y python36u python36u-libs python36u-devel python36u-pip
yum install -y git dbus systemd
timedatectl set-timezone Asia/Taipei
mkdir -p /usr/local/scripts && cd "$_"
git clone https://gitlab.com/miyorineko/minorinworker.git
cd minorinworker
pip3.6 install hug -U
pip3.6 install -r requirements.txt
python3.6 init.py
bash <(curl -L -s https://install.direct/go.sh)
systemctl daemon-reload
systemctl start minoworker